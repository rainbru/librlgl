# RLGL - the Rainbru's Landscape and Geometry Langage

The aim of this document is to present text files used in the language
definition.

## Common aspect to all parsers

It's a specialized C language.

### Standard types

<dl>
  <dt><strong>Boolean value</strong></dt>
  <dd>true|false just like in C;</dd>

  <dt><strong>integer/float</strong></dt>
  <dd>Like in C;</dd>

  <dt><strong>string</strong></dt>
  <dd>Double quoted : "aze";</dd>
  
  <dt><strong>Position</strong></dt>
  <dd>int_int</dd>

  <dt><strong>Identifier/group</strong></dt>
  <dd>/Identifier with space/{ ...}</dd>

</dl>

## MD - Map Definition

This defines the overall map for a given server. This is in the top-level
dir and is the sarting point of the parser.

	# This is a comment
	map("Name of the map")
	{
		players
		{
			spawn=Point;       # Defines the start point of all players
		}
			
		landscapes
		{
			dir= "landscapes/";
            size=250,250;        # The size of the all generated landscape
		}
	}

Landscape files will be called *{landscape_dir}/xxx-yyy.ld* starting at
"000-000.ld" **TODO: how to handle negative numbers ?**, maybe with letters ?

## LD - Landscape definition

Depending on the map generation used. Basically :

	landscape("name")
	{
		version = 12;            # The determinist random generator version
	    seed =12532152225;       # The srand seed
		points = 800;            # Number of points to be generated
		ambient = 1.0, 1.0, 1.0; # The ambient light definition (white)
	}
