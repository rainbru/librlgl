/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


/* This is a MapGenerator example  :
 * - Finally, ti's more a LoadingBarListener example, trying to fix
 *   
 */

#include "MapGenerator.hpp"
#include "LoadingBarListener.hpp"
#include "RlglSystem.hpp"

#include <iostream>

using namespace RLGL;
using namespace std;

class Loading : public LoadingBarListener
{
public:
  Loading(){  };
  virtual ~Loading(){};
  
  // Overides from LoadingBarListener
  virtual void addStep(const std::string& s){
    msgit = vMsg.begin();
    
    cout << "(Adding step " << s << ")" << endl;
    vMsg.push_back(s);
  };
  virtual void step(){
    if (msgit == vMsg.begin())
      cout << "* Starting map generation" << endl;

    if (msgit == vMsg.end())
      cout << "WARNING: last of steplist reached!!!" << endl;
    
    cout << "- " << (*msgit) << endl;
    ++msgit;
  };
  virtual void loadingDone()
  {
    cout << "* Map generation done" << endl;
  };

private:
  list<string> vMsg;
  list<string>::const_iterator msgit;
};

int
main()
{
  try
    {
      Loading l;
      System rSys("example-mapgenerator");
      MapGenerator* g = rSys.getGenerator();
      g->setMapName("example-mapgenerator");
      g->setSeed(123123);
      g->subscribe(&l);
      g->generate();
    }
  catch(const char* e)
    {
      cout << e << endl;
    }
  
  return 0;
}
