/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FifoObject.hpp"

#include "FifoStream.hpp"

#include <boost/filesystem.hpp>
#include <iostream>

using namespace std;

/*
  This is basically the failling tests.
 */

using namespace std;
using namespace RLGL;

// A simple override
class _FifoObject : public FifoObject
{
public:
  _FifoObject():FifoObject(FOT_INTEGER){};
  virtual ssize_t to_f(Fifo&)   { return 0; };
  virtual ssize_t from_f(Fifo&) { return 0; };

  size_t _write_type(Fifo&f){ return FifoObject::write_type(f); }

  void            setType(tFifoObjectType t){type = t;    };
  tFifoObjectType getType()                 {return type; };
  
};


int
main()
{
  try{
  string path = "/tmp/write_read-test";

      Fifo fr(path);
      fr.open(READ);
      Fifo fw(path);
      fw.open(WRITE);
      _FifoObject fo;
      size_t written = fo._write_type(fw);
      cout << "Correctly written " << written << " bytes." << endl;
    }
  catch (const char* e)
    {
      cout << "Error writting fifo" << endl;
      cout << e << endl;
    }
      
  return 0;
}
