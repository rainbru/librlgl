/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef _HASH_MAP_GROUP_HPP_
#define _HASH_MAP_GROUP_HPP_

#include <list>
#include <string>

using namespace std;

namespace RLGL
{
  /** The class currently responsible of the HashMap group feature
    *
    * Each key of the map can be retrieved using a list of group separated 
    * with "::". For example "map::direction::north".
    *
    */
  class HashMapGroup
  {
  public:
    HashMapGroup();
    virtual ~HashMapGroup();

    void addGroup(const string&);
    void popGroup();

    const string& current();
    string keyName(const string&);

    int size()const;

  private:
    list<string> mGroups;
    string lastCurrent; // Last generated current string
  };
}

#endif  //!_HASH_MAP_GROUP_HPP_
