/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "IndexFile.hpp"

#include <iostream>
#include <fstream>

#include "FileWritter.hpp"
#include "FileSystem.hpp" // Uses ::join()

using namespace std;

/** IndexFile constructor
  *
  * \param mapDir  The prefixed map directpry without the final separator.
  * \param mapName The full map name as entered by the user.
  *
  */
RLGL::IndexFile::IndexFile(const string& mapDir, const string& mapName):
  mMapDirectory(mapDir),
  mMapName(mapName),
  mFilename(FileSystem::join(mapDir, FILENAME))
{

}
  
RLGL::IndexFile::~IndexFile()
{
  
}

/** Get the filename of the index file
  *
  * It should be map directory + FILENAME.
  *
  */
const std::string&
RLGL::IndexFile::getFilename()const
{
  return mFilename;
}

/** Generate the sta,dard index file
  *
  *
  *
  */
void
RLGL::IndexFile::generate()
{
  ofstream of(mFilename.c_str());
  FileWritter fw(of);
  fw.startNamedGroup("map", mMapName);
  {
    fw.startGroup("players");
    fw << "spawn = 0, 0;" << EOL;
    fw.endGroup();
    fw << EOL;
    fw.startGroup("landscapes");
    fw << "dir = \"landscapes/\";" << EOL;
    fw << "size = 250, 250;" << EOL;
    fw.endGroup();

  }
  fw--;
  fw << "}" << EOL;
  of.close();
}
  
