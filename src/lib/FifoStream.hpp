/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FIFO_STREAM_HPP_
#define _FIFO_STREAM_HPP_

#include <map>
#include <string>

#include "Fifo.hpp"
#include "FifoObjectFactory.hpp"

using namespace std;

// Forward declations
namespace RLGL
{
  class FifoObject;
}
// And of forward declations

namespace RLGL
{

  /** A stream-like object used to serialize/deserialize from a Fifo
    *
    * This class is basically a hashmap.
    *
    */
  class FifoStream
  {
  public:
    FifoStream();
    ~FifoStream();

    FifoStream& push(std::string, RLGL::FifoObject*);
    RLGL::FifoObject* get(const std::string&);
    

    ssize_t write(RLGL::Fifo&);
    ssize_t read(RLGL::Fifo&);
    
    /** Return the size of the underlying map
      *
      */
    unsigned int size() const;
    
  private:
    std::map<std::string, FifoObject*> mMap;
    FifoObjectFactory factory;
  };

}

#endif // !_FIFO_STREAM_HPP_
