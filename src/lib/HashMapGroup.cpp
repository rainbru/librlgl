/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "HashMapGroup.hpp"

#include <sstream>

#include <boost/algorithm/string/join.hpp>

RLGL::HashMapGroup::HashMapGroup()
{

}

RLGL::HashMapGroup:: ~HashMapGroup()
{

}


/** Add a group to the current list */
void
RLGL::HashMapGroup::addGroup(const string& gr)
{
  mGroups.push_back(gr);
}

/** Remove the last added group */
void
RLGL::HashMapGroup::popGroup()
{
  mGroups.pop_back();
}

/** Returns the current groups */
const string&
RLGL::HashMapGroup::current()
{
  lastCurrent = boost::algorithm::join(mGroups, "::");
  return lastCurrent;
}

/** Returns the the key inside the current groups */
string
RLGL::HashMapGroup::keyName(const string& key)
{
  list<string> l (mGroups);
  l.push_back(key);
  return boost::algorithm::join(l, "::");
}

int
RLGL::HashMapGroup::size()const
{
  return mGroups.size();
};
