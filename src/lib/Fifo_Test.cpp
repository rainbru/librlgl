/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Fifo.hpp"

#include "CreateFifoException.hpp"

#include <gtest/gtest.h>

#include <boost/filesystem.hpp>

using namespace std;
using namespace RLGL;

// Test if it returns something
TEST( Fifo, pathname )
{
  Fifo f("/tmp/path");
  assert(f.getPathname() == "/tmp/path");
}

// Shouldn't open with incorrect mode
TEST( Fifo, open_throw )
{
  Fifo f("/tmp/path");
  ASSERT_ANY_THROW(f.open(UNSET));
}

// Test that calling mkfifo() with unexisting path throws an exception
TEST( Fifo, open_throw_mkfifo_error )
{
  Fifo f("/tmp/unexisting_path/fifo");
  ASSERT_ANY_THROW(f.open(UNSET));
}

// Opening in read mode first : should create the fifo
TEST( Fifo, open_read_create )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ); // Open for reading first
  // Simply test that the file exists
  EXPECT_TRUE(boost::filesystem::exists(path));
  boost::filesystem::remove(path);
  EXPECT_FALSE(boost::filesystem::exists(path));
}

// Shouldn't block the full test suite
TEST( Fifo, open_read_and_write )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ); // Open for reading first

  Fifo fw(path);
  fw.open(WRITE); // Open for writting

  // Make sure the FIFO is deleted
  boost::filesystem::remove(path);
}

// Deleting a reader should delete underlying FIFO object
TEST( Fifo, delete_read_remove_fifo )
{
  string path = "/tmp/path";
  Fifo* f = new Fifo(path);
  f->open(READ);
  EXPECT_TRUE(boost::filesystem::exists(path));
  delete f;
  EXPECT_FALSE(boost::filesystem::exists(path));

}

// Trying to write an int to the fifo
TEST( Fifo, write_test_int )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ);

  Fifo fw(path);
  fw.open(WRITE);

  ssize_t ret = fw.write<int>(5);
  EXPECT_EQ(ret, sizeof(int));
}

// Trying to write an int to the fifo
TEST( Fifo, write_read_int )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ);

  Fifo fw(path);
  fw.open(WRITE);

  ssize_t ret = fw.write<int>(5);
  int reti;
  size_t ret2= f.read<int>(&reti);
  EXPECT_EQ(ret, sizeof(int));
  EXPECT_EQ(ret2, sizeof(int));
  EXPECT_EQ(reti, 5);
  
}

// Now should simply delete it
TEST( Fifo, open_create_two_error )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ);

  Fifo f2(path);
  f2.open(READ);
}

// Trying to write to a FIFO in read mode should throw an exception
TEST( Fifo, write_on_reader )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ);

  ASSERT_THROW(f.write<int>(8), BadFifoModeException);
}

// Trying to read from a FIFO in write mode should throw an exception
TEST( Fifo, read_on_writer )
{
  // We have to create the fifo first to avoid an "Unknown file exception"
  string path = "/tmp/path";
  Fifo fr(path);
  fr.open(READ);

  Fifo f(path);
  f.open(WRITE);

  int i;
  ASSERT_THROW(f.read<int>(&i), BadFifoModeException);
}

// A full int test
TEST( Fifo, full_test_int )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ);

  Fifo fw(path);
  fw.open(WRITE);

  int i1 = 5, i2;
  
  ssize_t ret_write = fw.write<int>(i1);
  ssize_t ret_read = f.read<int>(&i2);

  EXPECT_EQ(ret_write, ret_read);
  EXPECT_EQ(i1, i2);
}

// A full int test
TEST( Fifo, full_test_bool )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ);

  Fifo fw(path);
  fw.open(WRITE);

  bool i1 = true, i2;
  
  ssize_t ret_write = fw.write<bool>(i1);
  ssize_t ret_read = f.read<bool>(&i2);

  EXPECT_EQ(ret_write, ret_read);
  EXPECT_EQ(i1, i2);
}

// A full int test
TEST( Fifo, full_test_float )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ);

  Fifo fw(path);
  fw.open(WRITE);

  float i1 = 3.14159, i2;
  
  ssize_t ret_write = fw.write<float>(i1);
  ssize_t ret_read = f.read<float>(&i2);

  EXPECT_EQ(ret_write, ret_read);
  EXPECT_EQ(i1, i2);
}

// Testing the writtent size for a string
TEST( Fifo, write_string_len )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ);

  Fifo fw(path);
  fw.open(WRITE);

  string s = "Hello world";
  ssize_t expected = sizeof(ssize_t) + (sizeof(char) * s.length());
  ssize_t ret_write = fw.write(s);

  EXPECT_EQ(ret_write, expected);
}

// A full string test
TEST( Fifo, full_test_string )
{
  string path = "/tmp/path";
  Fifo f(path);
  f.open(READ);

  Fifo fw(path);
  fw.open(WRITE);

  string s = "Hello world", s2;
  ssize_t ret_write = fw.write(s);
  ssize_t ret_read = f.read(s2);

  EXPECT_EQ(ret_write, ret_read);
  EXPECT_EQ(s, s2);
}

