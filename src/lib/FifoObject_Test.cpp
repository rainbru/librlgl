/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FifoObject.hpp"

#include "FifoStream.hpp"

#include <gtest/gtest.h>

#include <boost/filesystem.hpp>

using namespace std;
using namespace RLGL;

// A simple override
class _FifoObject : public FifoObject
{
public:
  _FifoObject():FifoObject(FOT_UNDEFINED){};
  virtual ssize_t to_f(Fifo&)   { return 0; };
  virtual ssize_t from_f(Fifo&) { return 0; };

  size_t _write_type(Fifo&f){

    // Do not try to try/catch exception or do not forget to chain it
    // Some following tests are using EXPECT_THROW and thus may fail
    // If we avoind lib's code throwing them.
    return FifoObject::write_type(f);
  }

  void            setType(tFifoObjectType t){type = t;    };
  tFifoObjectType getType()                 {return type; };
  
};

// Test if it returns something, if the override works
TEST( FifoObject, simple )
{
  _FifoObject fo;
}

// Trying to write an undefined type throws an error
TEST( FifoObject, write_type_throw )
{
  string path = "/tmp/write_read-test";
  Fifo fr(path);
  fr.open(READ);

  Fifo fw(path);
  fw.open(WRITE);

  _FifoObject fo;
  //  ASSERT_ANY_THROW(size_t written = fo._write_type(fw));
  EXPECT_THROW(fo._write_type(fw), runtime_error);
}

// Test that writting a type give the good length
TEST( FifoObject, write_type )
{
  string path = "/tmp/write_read-test";
  Fifo fr(path);
  fr.open(READ);
  
  Fifo fw(path);
  fw.open(WRITE);
  
  _FifoObject fo;
  fo.setType(FOT_INTEGER);
  size_t written = fo._write_type(fw);
  
  EXPECT_EQ(written, 16);
}
