/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _LOCAL_WORLDS_LISTENER_HPP_
#define _LOCAL_WORLDS_LISTENER_HPP_

#include <string>

namespace RLGL
{
  /** Define a subscriber for the local worlds parsing
    *
    * - Reimplement/override this class;
    * - subscribe to the RlglSystem publisher;
    * - then call RlglSystem::parseLocalWorlds();
    *
    */
  class LocalWorldsListener
  {
  public:
    virtual ~LocalWorldsListener(){};
    
    virtual void parsingStarted(int nbworld)=0; //!< Get the number of worlds
    virtual void parsingFinished()=0;           //!< Parsing ends
    /** We parsed a new world
      *
      * \param worldName the newly parsed world name
      *
      */
    virtual void gotWorld(const std::string& worldName)=0;
  };

}

#endif // !_LOCAL_WORLDS_LISTENER_HPP_
