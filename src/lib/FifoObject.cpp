/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FifoObject.hpp"

#include "fo/Type.hpp"

RLGL::FifoObject::FifoObject(tFifoObjectType t):
  type(t)
  //  type(FOT_UNDEFINED)
{

}

RLGL::FifoObject::~FifoObject()
{

}

/** Write the type of this object to the given fifo
  *
  * All subclass (but the Type itself obviously), must call this before 
  * writting their own value.
  *
  *     FifoObject::write_type(FIFO)
  *
  */
ssize_t
RLGL::FifoObject::write_type(RLGL::Fifo& f)
{
  if (type == FOT_UNDEFINED)
    throw runtime_error("Cannot serialize an undefined type");
  
  foType ft(type);
  return f.write(ft);
}
