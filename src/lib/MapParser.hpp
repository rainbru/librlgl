/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MAP_PARSER_HPP_
#define _MAP_PARSER_HPP_

#include <string>
#include <boost/spirit/include/classic.hpp>

#include "ParserBase.hpp"

namespace RLGL
{

  /** A base class for all RLGL parsers
    *
    * Defines some useful base for all parser (Identifier etc...)
    *
    */
  class MapParser: public ParserBase
  {
  public:
    explicit MapParser(const std::string& path);
    ~MapParser();

    virtual void parse();
  };

}

#endif // !_MAP_PARSER_HPP_
