/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FIFO_HPP_
#define _FIFO_HPP_

#include "ClosingFifoException.hpp"

#include <string>
#include <unistd.h> // Uses write()

#include <iostream>


using namespace std;

namespace RLGL
{

  class BadFifoModeException : public exception
  {
  public:
    BadFifoModeException(){};
  };

  
  /** A fifo open mode
    *
    */
  typedef enum
    {
      READ = 1,
      WRITE,
      UNSET
    }tFifoOpenMode;

  /** A non-blocking fifo class
    *
    * We must create the reader first, then create the writter. 
    *
    * The underlying fifo object is deleted when the reader is deleted.
    *
    */
  class Fifo
  {
  public:
    explicit Fifo(const string&);
    ~Fifo();

    void open(tFifoOpenMode);
    void close();
    
    const string& getPathname()const;
    bool exists();
    ssize_t write(const std::string&);
    ssize_t read(std::string& s);

    /** Returns the number of written bytes */
    template<typename T> ssize_t write(T val)
    {
      if (mMode != WRITE)
	throw BadFifoModeException();
      else
	cout << "mfileDescriptor is " << mfileDescriptor << endl;
	return ::write(mfileDescriptor, &val, sizeof(T));
    }

    /** Returns the number of bytes that were read */
    template<typename T> ssize_t read(T* val)
    {
      if (mMode != READ)
	throw BadFifoModeException();
      else
	return ::read(mfileDescriptor, val, sizeof(T));
    }

  private:
    string path;         //!< The pathname of the fifo
    tFifoOpenMode mMode;  //!< The current mode
    int mfileDescriptor; // The C fifo fd
    bool opened;         // Is the C fifo currently opened ?
  };
}

#endif // !_FIFO_HPP_
