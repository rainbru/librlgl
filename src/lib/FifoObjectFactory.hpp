/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FIFO_OBJECT_FACTORY_HPP_
#define _FIFO_OBJECT_FACTORY_HPP_

#include "FifoObject.hpp"
#include "fo/Type.hpp"
#include <map>

namespace RLGL
{
  /** The method to create FifoObjects */
  typedef FifoObject* (*factoryMethod)();
  
  /** A factory used to deserialize FifoObject from its type
    *
    */
  class FifoObjectFactory
  {
  public:
    size_t size() const;
    void registerObject(foType, factoryMethod);
    FifoObject* create(foType);
    
  private:
    std::map<uint8_t, factoryMethod> registeredTypes;
  };

}

#endif // !_FIFO_OBJECT_FACTORY_HPP_
