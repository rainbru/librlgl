/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FifoStream.hpp"

#include "Fifo.hpp"

#include "fo/Integer.hpp"
#include "fo/Float.hpp"
#include "fo/String.hpp"

#include <gtest/gtest.h>

using namespace RLGL;

TEST( FifoStream, ctor )
{
  FifoStream fs;
  EXPECT_EQ(fs.size(), 0);
}

// Really add to the map
TEST( FifoStream, push )
{
  FifoStream fs;
  foInteger fi;
  fs.push("aze", &fi);
  EXPECT_EQ(fs.size(), 1);
}

// Make sure we can 'chain' push calls
TEST( FifoStream, push_return )
{
  FifoStream fs;
  foInteger fi;
  fs.push("aze", &fi).push("zer", &fi);
  EXPECT_EQ(fs.size(), 2);
}

// Make sure we can get object back
TEST( FifoStream, push_get )
{
  FifoStream fs;
  foInteger fi(5);
  fs.push("aze", &fi).push("zer", &fi);

  foInteger* fo = dynamic_cast<foInteger*>(fs.get("aze"));
  EXPECT_EQ(fo->value(), 5);
}

// Full write then read test of a complete map
TEST( FifoStream, write_read )
{
  size_t written, read;
  
  string path = "/tmp/write_read-test";
  Fifo fr(path);
  fr.open(READ);

  Fifo fw(path);
  fw.open(WRITE);

  // Writing
  FifoStream fs;
  foInteger fi(5);
  foFloat ff(2.34);
  foString fstr("testStr");
  fs.push("int", &fi).push("float", &ff).push("str", &fstr);
  written = fs.write(fw);

  // Reading
  FifoStream fsr;
  try {
    // May be disabled due to "unknown file: Failure"
    read = fs.read(fr);
  }
  catch (std::exception e) {
    cout << "Catched exception :"  <<  e.what() << endl;
  }
  
  // foInteger* fo = dynamic_cast<foInteger*>(fs.get("aze"));
  EXPECT_EQ(written, 62);
  EXPECT_EQ(written, read);
}

