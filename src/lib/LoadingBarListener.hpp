/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _LOADING_BAR_LISTENER_HPP_
#define _LOADING_BAR_LISTENER_HPP_

#include <string>

namespace RLGL
{
  /** Define a subscriber for the a loading bar
    *
    * To be able to use this listener, you must follow this implementation :
    * - Before starting the long action, the user will add all steps, passing
    *   as parameter the step name. This is to the implementer to compute
    *   the total number of steps and the value of each one;
    * - When the action start, the step() function is called for each 
    *   passed step, telling the subscriber to switch to next one.
    *
    * - Reimplement this class;
    * - subscribe to the MapGenerator class. You can get an instance from 
    *   the RlglSystem getter;
    * - call the generate or load function.
    *
    */
  class LoadingBarListener
  {
  public:
    virtual ~LoadingBarListener(){};

    /** This function will be called for each step before long work start
      *
      * \param stepName The name of the step
      *
      */
    virtual void addStep(const std::string& stepName)=0;

    /** Mar a step as finished
      *
      * Will be called after each step.
      *
      */
    virtual void step()=0;

    /** This function will be called when loading's done. Action is completed.
      *
      *
      */
    virtual void loadingDone()=0;
  };

}

#endif // !_LOADING_BAR_LISTENER_HPP_
