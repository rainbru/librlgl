/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FifoObjectFactory.hpp"
#include "fo/Type.hpp"
#include "fo/Integer.hpp"

#include <gtest/gtest.h>
#include <string>

using namespace RLGL;
using namespace std;

// Empty registration function
RLGL::FifoObject* fun(){return NULL;}

TEST( FifoObjectFactory, size )
{
  FifoObjectFactory fof;
  EXPECT_EQ( fof.size(), 0);
}

TEST( FifoObjectFactory, register )
{
  FifoObjectFactory fof;
  fof.registerObject(foType(1), &fun);
  EXPECT_EQ( fof.size(), 1);
}

// Trying to register the same type twice should throw an exception
TEST( FifoObjectFactory, register_twice )
{
  FifoObjectFactory fof;
  fof.registerObject(foType(1), &fun);
  ASSERT_ANY_THROW(fof.registerObject(foType(1), &fun));
}


TEST( FifoObjectFactory, create )
{
  FifoObjectFactory fof;
  fof.registerObject(FOT_INTEGER, &foInteger::factoryMethod);
  foInteger* fi = dynamic_cast<foInteger*>(fof.create(FOT_INTEGER));
  ASSERT_TRUE( fi != NULL);
  
}

TEST( FifoObjectFactory, create_unknown )
{
  FifoObjectFactory fof;
  fof.registerObject(FOT_INTEGER, &foInteger::factoryMethod);
  ASSERT_ANY_THROW(fof.create(8));
}
