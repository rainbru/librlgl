/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "MapGenerator.hpp"

#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <math.h>       /* round, floor, ceil, trunc */

#include "RlglSystem.hpp"
#include "FileSystem.hpp"
#include "RandomNumber.hpp"
#include "LoadingBarListener.hpp"

#include "IndexFile.hpp"

#include <unistd.h>
#include <boost/format.hpp> 
#include <sstream> 

RLGL::MapGenerator::MapGenerator(RLGL::System* sys,int w, int h):
  mSystem(sys),
  mSeed(-1)
{
  /*  int version = 1;        // The random generator version
      int points = (w+h) * 2; // The number of generated points
  */
}

RLGL::MapGenerator::~MapGenerator()
{

}

void
RLGL::MapGenerator::subscribe(RLGL::LoadingBarListener* lbl)
{
  mLoadingBarListeners.push_back(lbl);

}

void
RLGL::MapGenerator::unsubscribe(RLGL::LoadingBarListener* lbl)
{
  mLoadingBarListeners.remove(lbl);
}

void
RLGL::MapGenerator::generate()
{
  notifyAddStep("Checking map name");
  
  notifyAddStep("Creating map directory");
  notifyAddStep("Generating map definition file");

  // Checking map name
  if (mMapName.empty())
    throw std::runtime_error("Generating a map with empty name");
  // Testing seed
  if (mSeed == -1)
    throw std::runtime_error("Generating a map with default seed");
  notifyStep();

  // Creating map directory
  mMapDirectory = FileSystem::createMapDirectory( mSystem->getPrefixedHome(),
						  mMapName);
  notifyStep();

  
  // Generate index file
  IndexFile ind(mMapDirectory, mMapName);
  ind.generate();
  notifyStep();

  // All work is done : notify listeners
  notifyDone();
}

void
RLGL::MapGenerator::notifyAddStep(const string& stepName)
{
  std::list<LoadingBarListener*>::iterator p;
  for(p = mLoadingBarListeners.begin(); p != mLoadingBarListeners.end(); ++p)
    (*p)->addStep(stepName);
}

void
RLGL::MapGenerator::notifyStep()
{
  std::list<LoadingBarListener*>::iterator p;
  for(p = mLoadingBarListeners.begin(); p != mLoadingBarListeners.end(); ++p)
    (*p)->step();
}

/** Return a resulting string of the format containing the integer
  *
  * \param fmt A format string that should contain %s
  * \param i   The integer passed to format string
  *
  */
string
RLGL::MapGenerator::nStep(const string& fmt, int i)
{
  ostringstream oss;
  oss <<  boost::format(fmt) % i;
  return oss.str();
}

/** Return a resulting string of the dormat containing two integers
  *
  * \param fmt A format string that should contain %s
  * \param i1  First integer to be passed to format string
  * \param i2  First integer to be passed to format string
  *
  */
string
RLGL::MapGenerator::nStep(const string& fmt, int i1, int i2)
{
  ostringstream oss;
  oss <<  boost::format(fmt) % i1 % i2;
  return oss.str();
}

void
RLGL::MapGenerator::setMapName(const std::string& mn)
{
  mMapName = mn;
}

/** Notify all listener that the loading is done
  *
  */
void
RLGL::MapGenerator::notifyDone()
{
  std::list<LoadingBarListener*>::iterator p;
  for(p = mLoadingBarListeners.begin(); p != mLoadingBarListeners.end(); ++p)
    (*p)->loadingDone();
}

/** Retrieve the newest generated directory
  *
  * When generating a new local map, you'll have to call setMapName() and
  * generate() before getting the full mapname.
  *
  */
const std::string&
RLGL::MapGenerator::getMapDirectory()
{
  return mMapDirectory;
}

void
RLGL::MapGenerator::setSeed(tSeed s)
{
  mSeed = s;
}

RLGL::tSeed
RLGL::MapGenerator::getSeed()const
{
  return mSeed;
}

/** Set the generation seed from a string
  *
  * \param str The seed integer as string.
  *
  * \throws basic_ios::clear If the string parameter can't be converted to 
  *         integer.
  *
  */
void
RLGL::MapGenerator::setSeed(const std::string& str)
{
  istringstream iss(str);
  iss.exceptions(std::ios::failbit);
  tSeed s;
  iss >> s;
  // Manually use setSeed in case we some tests
  setSeed(s);
}
