/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "RandomNumber.hpp"

#include <sstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#include "config.h"

/** Create a generator using given seed and version
  *
  * \param seed    The seed
  * \param version The generator version
  *
  */
RLGL::RandomNumber::RandomNumber(time_t seed, int version ):
  mVersion(version),
  next(0)
{
  try{
    load(filename(version));
    if (!mNumbers.empty())
      next = seed % mNumbers.size();
  }
  catch (...)
    {
      cout << "Something went wrong" << endl;
    }
}

/** Get the data filename from the generator version
  *
  * \param version The generator version
  *
  */
string
RLGL::RandomNumber::filename(int version)
{
  ostringstream oss;
  oss << ROOTDIR << "/data/numbers." << versionToStr(version);
  return oss.str();
}


/** Output to cout nb numbers according to the current seed
  *
  * This function is only used to generate new data file. It uses
  * srand() and rand() to generate real random numbers, not 
  * platform-independant ones.
  *
  * \param nb The number of integer to be printed
  *
  */
void
RLGL::RandomNumber::generate(int nb)
{
  srand(time(NULL));
  for (int i=0; i<nb;++i)
    {
      cout << rand() << endl;
    }
  
}

string
RLGL::RandomNumber::versionToStr(int i)
{
  ostringstream ss;
  ss << setw(3) << setfill('0') << i;
  return ss.str();
}

/** Load the given file
  *
  */
void
RLGL::RandomNumber::load(const string& filename)
{
  string line;
  
  ifstream myfile(filename.c_str());
  if (myfile.is_open())
  {
    int num;
    while ( getline (myfile,line) )
    {
      stringstream st;
      st << line;
      st >> num;
      mNumbers.push_back(num);
    }
    myfile.close();
  }
  else cout << "Unable to open file " << filename; 
}

int
RLGL::RandomNumber::get()
{
  if (++next > mNumbers.size())
    next = 0;
  return mNumbers[next];
}
