/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _RLGL_MAP_GEENRATOR_HPP_
#define _RLGL_MAP_GEENRATOR_HPP_

#include <list>
#include <vector>
#include <string>

using namespace std;

namespace RLGL
{

  // Forward declarations
  class System;
  class LoadingBarListener;
  // And of forward declarations

  /** Represents a 2D point */
  typedef struct
  {
    int x; //!< The X axis value
    int y; //!< The Y axis value
  } tPoint;

  /** The MapGenerator::mSeed type */
  typedef long int tSeed;

  /** A Map generator 
    *
    * This class uses PubSub design pattern to notify client.
    *
    */
  class MapGenerator
  {
  public:
    MapGenerator(RLGL::System*, int, int);
    ~MapGenerator();

    void setMapName(const std::string&);
    const std::string& getMapDirectory();

    void setSeed(tSeed);
    void setSeed(const std::string&);
    RLGL::tSeed getSeed()const;
    
    // PubSub related functions
    void subscribe(RLGL::LoadingBarListener*);
    void unsubscribe(RLGL::LoadingBarListener*);
    void generate();

  protected:
    void notifyAddStep(const string&);
    void notifyStep();
    void notifyDone();

    string nStep(const string&, int);
    string nStep(const string&, int, int);
    
  private:
    RLGL::System* mSystem;     //!< Mainly to get prefix
    vector<tPoint> mPoints;    //!< A vector of tPoint
    std::list<LoadingBarListener*> mLoadingBarListeners; //!< Some listeners
    std::string mMapName;      //!< The map name
    std::string mMapDirectory; //!< The map directory name
    RLGL::tSeed mSeed;         //!< The new map's seed
  };
}

#endif // !_RLGL_MAP_GEENRATOR_HPP_
