/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "MapParser.hpp"

using namespace std;

#define WORLD_INDEX "_index.md"

RLGL::MapParser::MapParser(const string& path):
  ParserBase(path, WORLD_INDEX)
{
  // file is 'filename'
}

RLGL::MapParser::~MapParser()
{

}

void
RLGL::MapParser::parse()
{
  // See http://stackoverflow.com/a/22657600
}
