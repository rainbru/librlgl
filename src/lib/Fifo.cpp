/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Fifo.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>  // Uses O_WRONLY and O_RDONLY
#include <unistd.h> // Uses close()

#include <iostream>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/exception.hpp>

#include "CreateFifoException.hpp"

namespace fs = boost::filesystem;

using namespace std;

RLGL::Fifo::Fifo(const string& pathname):
  path(pathname),
  mMode(UNSET),
  mfileDescriptor(0),
  opened(false)
{

}

RLGL::Fifo::~Fifo()
{
  if (opened)
    close();
}

const string&
RLGL::Fifo::getPathname()const
{
  return path;
}


void
RLGL::Fifo::open(RLGL::tFifoOpenMode mode)
{
  mMode = mode;
  if (mode == WRITE)
    {
      
      // We have to qualify open() call here. We don't
      // recursively call Fifo::open  but we call open function
      // from anon namespace
      mfileDescriptor = ::open(path.c_str(), O_WRONLY);
      if (mfileDescriptor == -1)
	throw "Error opening fifo";
    }
  else if (mode == READ)
    {
      // If the path exist, delete it
      if ( boost::filesystem::exists( path ) )
	{
	  try
	    {
	      boost::filesystem::remove(path);
	    }
	  catch (std::exception const& e)
	    {
	      std::cerr << e.what() << '\n';
	    }
	}
      
      // Create fifo 
      int ret = mkfifo(path.c_str(), 0666);
      if (ret == -1)
	throw CreateFifoException(ret);
      
      mfileDescriptor = ::open(path.c_str(), O_RDONLY | O_NONBLOCK );
      if (mfileDescriptor == -1)
	throw "Error opening fifo";
    }
  else
    {
      throw("Invalid fifo mode");
    }

  
  if (mfileDescriptor != -1)
    {

    opened = true;
    }
  
}

/** Close and delete the fifo
  *
  */
void
RLGL::Fifo::close()
{
  int c = ::close(mfileDescriptor);
  if (c != 0)
    {
      throw ClosingFifoException(errno);
    }
  
  // Only remove FIFO if we're the reader
  if (mMode == READ)
    {
      unlink(path.c_str());
    }
  opened = false;
}

/** Test if the underlying FIFO file exist
  *
  */
bool
RLGL::Fifo::exists()
{
  return fs::exists(path);
}

/** Write the given string to the FIFO
  * 
  * \param s The string to be written
  *
  * \return The number of written bytes
  *
  */
ssize_t
RLGL::Fifo::write(const std::string& s)
{
  ssize_t written = write<size_t>(s.length());
  written += ::write(mfileDescriptor, s.c_str(), s.length());
  return written;
}

/** Read a string from the FIFO
  *
  * \param s The string object used to store read content
  *
  * \return The number of read bytes
  *
  */
ssize_t
RLGL::Fifo::read(std::string& s)
{
  // First, read the string size
  size_t len;
  ssize_t re = read<size_t>(&len);

  if (re < 0)
    throw("Error while reading string lenght");

  if (len > s.max_size())
    throw("Max string size exceeded");
  
  // Then, read the string content
  char buf[len];
  re += ::read(mfileDescriptor, buf, len);
  s = string(buf, len);
  return re;
}
