/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _INDEX_FILE_HPP_
#define _INDEX_FILE_HPP_

#include <string>

#define FILENAME "index.md"

namespace RLGL
{

  /** Generates the map's index file
   * 
   *
   *
   */
  class IndexFile
  {
  public:
    IndexFile(const std::string&, const std::string&);
    ~IndexFile();
    
    void generate();

    const std::string& getFilename()const;
    
  private:
    std::string mMapDirectory;
    std::string mMapName;

    std::string mFilename;
    
  };
  
} /* !namespace RLGL */
#endif // !_INDEX_FILE_HPP_
