/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FileHandler.hpp"

#include <gtest/gtest.h>

using namespace RLGL;

// Test if no autoload gets an empty typehandler
TEST( FileHandler, empty )
{
  FileHandler fh(false);
  EXPECT_EQ( fh.size(), 0 );
}

// Test if adding a typehandler makes it exist ion the map
TEST( FileHandler, exits )
{
  FileHandler fh(false);
  fh.addTypeHandler(".aze", FHT_MAP_DEFINITION);
  ASSERT_TRUE( fh.exists(".aze"));
}


// Test if adding a typehandler adds an element to the map
TEST( FileHandler, add_one )
{
  FileHandler fh(false);
  fh.addTypeHandler(".aze", FHT_MAP_DEFINITION);
  EXPECT_EQ( fh.size(), 1 );
}

// Test oif autoload adds at least one extension
TEST( FileHandler, autoload )
{
  FileHandler fh;
  EXPECT_GT( fh.size(), 0 );
}

TEST( FileHandler, already_exists )
{
  FileHandler fh(false);
  fh.addTypeHandler(".aze", FHT_MAP_DEFINITION);
    
  ASSERT_ANY_THROW( fh.addTypeHandler(".aze", FHT_MAP_DEFINITION));
}

TEST( FileHandler, getext_1 )
{
  FileHandler fh;
  string ex1 = fh.getExtension("filename.aze");
  EXPECT_EQ( ex1, ".aze" );
}

TEST( FileHandler, getext_none )
{
  // Shoudn't return an extension
  FileHandler fh;
  string ex1 = fh.getExtension("filename");
  EXPECT_EQ( ex1, "" );
}

TEST( FileHandler, getext_more_dots )
{
  // Shoudn't return an extension
  FileHandler fh;
  string ex1 = fh.getExtension("filename.tyu.zer.aze");
  EXPECT_EQ( ex1, ".aze" );
}

TEST( FileHandler, getype_1 )
{
  // Shoudn't return an extension
  FileHandler fh;
  fh.addTypeHandler(".aze", FHT_MAP_DEFINITION);
  RLGL::tFileHandlerType t = fh.getType("filename.aze");
  EXPECT_EQ( t, FHT_MAP_DEFINITION );
}

TEST( FileHandler, getype_none )
{
  // Shoudn't return an extension
  FileHandler fh;
  fh.addTypeHandler(".aze", FHT_MAP_DEFINITION);
  RLGL::tFileHandlerType t = fh.getType("filename");
  EXPECT_EQ( t, FHT_NONE );
}
