/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _SEED_HPP_
#define _SEED_HPP_

#include <string>

namespace RLGL
{
  /** A class used to handle world seed
    *
    * Use this class to generate, check or handle Seed.
    *
    */
  class Seed{
  public:
    Seed();
    ~Seed();
    
    void randomize();
    int to_i() const;
    std::string to_s() const;
    
  private:
    int seed;   //!< The underlaying integer
  };
}

#endif // !_SEED_HPP_
