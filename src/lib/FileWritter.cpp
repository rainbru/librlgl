/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FileWritter.hpp"

#include <sstream>  // Uses ostringstream

using namespace std;

RLGL::FileWritter::FileWritter(std::ostream& str):
  indentLevel(0),
  indentValue(2),
  lastChar(' '),
  mStream(str.rdbuf())
{
  
}
  
RLGL::FileWritter::~FileWritter()
{

}


void RLGL::FileWritter::indent()
{
  for (int i = 0; i < indentLevel; i++)
    mStream << ' ';
}
	       
RLGL::FileWritter&
RLGL::FileWritter::operator++(int)
{
  indentLevel+=indentValue;
  return *this;
}
	       
RLGL::FileWritter&
RLGL::FileWritter::operator--(int)
{
  indentLevel-=indentValue;
  if (indentLevel < 0)
    indentLevel = 0;
  return *this;

}
	       
RLGL::FileWritter&
RLGL::FileWritter::operator<<(char c)
{
  mStream << c;
  lastChar = c;
  return *this;
}


RLGL::FileWritter&
RLGL::FileWritter::startGroup(const std::string& gr)
{
  (*this) << gr << EOL;
  (*this) << "{" << EOL;
  (*this)++;
  return (*this);
}

RLGL::FileWritter&
RLGL::FileWritter::startNamedGroup(const std::string& group,
				   const std::string& name)
{
  ostringstream oss;
  oss << group << "(\"" << name << "\")";
  return startGroup(oss.str());
}


RLGL::FileWritter&
RLGL::FileWritter::endGroup()
{
  (*this)--;
  (*this) << "}" << EOL;
  return (*this);
}

RLGL::FileWritter&
RLGL::FileWritter::operator++()
{
  indentLevel+=indentValue;
  return *this;
}
	       
RLGL::FileWritter&
RLGL::FileWritter::operator--()
{
  indentLevel-=indentValue;
  if (indentLevel < 0)
    indentLevel = 0;
  return *this;

}
