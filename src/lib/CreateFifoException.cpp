/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "CreateFifoException.hpp"

#include "IntCodeException.hpp"

#include <sstream>

RLGL::CreateFifoException::CreateFifoException(int code):
  IntCodeException(code, "creating FIFO")
{
  ostringstream oss;
  oss << "Error creating FIFO : (" << code << "): " << codeToStr(code);
  mWhat = oss.str();
  
}

string
RLGL::CreateFifoException::codeToStr(int) const
{
  string ret;
  switch (mCode)
    {
    case EBADF:
      ret = "The files argument is not a valid file descriptor.";
      break;
    case EINTR:
      ret = "The close() function was interrupted by a signal.";
	break;
    case EIO:
      ret = "An I/O error occurred while reading from or writing to FS.";
      break;
    case -1:
      ret = "Maybe an already existing file ?";
      break;
    default:
      ret = "Unknown error";
      
    }
  return ret;
}
