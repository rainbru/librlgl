/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FileSystem.hpp"

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include <cstdlib> // Uses getenv
#include <algorithm> // Uses std::replace
#include <boost/algorithm/string/replace.hpp>  // Uses boost::replace_all
#include <string>
#include <iostream>
#include <sstream>

#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/locale.hpp>

using namespace std;
using namespace boost;

RLGL::FileSystem::FileSystem()
{
  
}

RLGL::FileSystem::~FileSystem()
{

}

std::string
RLGL::FileSystem::getHomeDir()
{
  const char *homedir = getenv("HOME");

  if (homedir == NULL) {
    homedir = getpwuid(getuid())->pw_dir;
  }
  string ret(homedir);
  return ret;
}


/**
 *
 * \param sep = the path separator
 */
std::string
RLGL::FileSystem::join(const std::string& str1, const std::string& str2,
		       char sep)
{
  ostringstream oss;

  if (*str1.rbegin() == sep || *str2.begin() == sep) 
    oss << str1 << str2;
  else
    oss << str1 << sep << str2;
  
  return oss.str();
}

std::string
RLGL::FileSystem::getUserDir(const std::string& prefix)
{
  string ret;

  // Add leading separator if not present
  string p; // New prefix
  if (*prefix.rbegin() == PATH_SEPARATOR)
    p = prefix;
  else
    p = join(prefix, ""); // Add the separator to the prefix
    
  return join(getHomeDir(), p);
}

/**
  * throw an boost::filesystem::directory_iterator::construct error if 
  * path doesn't exist
  *
  */
RLGL::tFileList
RLGL::FileSystem::subdirs(const std::string& path)
{
  tFileList ret;
  
  filesystem::path filepath(path);
  filesystem::directory_iterator it(filepath);
  filesystem::directory_iterator end;
  BOOST_FOREACH(const filesystem::path& p, make_pair(it, end))
    {
      ret.push_back(p.string());
    }
  return ret;
}

void
RLGL::FileSystem::createPathIfNeeded(boost::filesystem::path p)
{
  filesystem::path parent = p.parent_path();
  if (!exists(parent))
    createPathIfNeeded(parent);

  if (!exists(p))
    {
      if (create_directory(p))
	cout << "Directory '" << p << "' succefully created" << endl;
      else
	cout << "Error creating '" << p << "' directory!" << endl;
    }
}

void
RLGL::FileSystem::createPathIfNeeded(const std::string& p)
{
  filesystem::path path(p);
  createPathIfNeeded(path);
}

std::string
RLGL::FileSystem::sanitize(const std::string& s)
{
  string ret = s;
  
  boost::replace_all(ret, "é", "e");
  boost::replace_all(ret, "è", "e");
  boost::replace_all(ret, "ê", "e");
  boost::replace_all(ret, "ë", "e");

  boost::replace_all(ret, "'", "_");
  boost::replace_all(ret, ",", "_");
  boost::replace_all(ret, ";", "_");
  boost::replace_all(ret, ".", "_");
  boost::replace_all(ret, "@", "_");
  boost::replace_all(ret, " ", "_");
  return ret;
}

/** Create a unique map directory
  *
  * \param parent  The parent directory, ideally a prefixed homedir.
  * \param mapname The user-supplied (i.e. not yet sanitized) map name
  *
  */
std::string
RLGL::FileSystem::createMapDirectory(const std::string& parent,
				     const std::string& mapname)
{
  int idx = 0;
  string path;
  filesystem::path bpath;

  string worlds = join(parent, "worlds");
  string path_base = join(worlds, sanitize(mapname));
  path = path_base;
  cout << "base path " << path_base << endl;
  bpath = path_base;
  while(exists(bpath))
    {
      ostringstream oss;
      oss << path_base << "_" << idx++;
      bpath = path = oss.str();
    }

  createPathIfNeeded(path);
  return path;
}
