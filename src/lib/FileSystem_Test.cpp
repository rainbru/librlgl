/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FileSystem.hpp"
#include "RlglSystem.hpp" // Uses the RLGL::System class

#include <gtest/gtest.h>
#include <string>
#include <sstream>

#include <boost/filesystem.hpp>

using namespace std;
using namespace RLGL;

/* Note: many of these tests will only pass on linux due to the 
 * PATH_SEPARATOR macro from FileSystem.hpp.
 *
 */

// Test if it returns something
TEST( FileSystem, empty )
{
  string s = FileSystem::getHomeDir();
  ASSERT_FALSE( s.empty() );
}

TEST( FileSystem, join_standard )
{
  string s = "aze";
  string s2 = "zef";
  ASSERT_EQ( FileSystem::join(s, s2), "aze/zef" );
}

TEST( FileSystem, join_custom )
{
  string s = "aze";
  string s2 = "zef";
  ASSERT_EQ( FileSystem::join(s, s2, '_'), "aze_zef" );
}

TEST( FileSystem, join_already_sep )
{
  string s = "aze/";
  string s2 = "zef";
  ASSERT_EQ( FileSystem::join(s, s2), "aze/zef" );
}

TEST( FileSystem, join_already_sep2 )
{
  string s = "aze";
  string s2 = "/zef";
  ASSERT_EQ( FileSystem::join(s, s2), "aze/zef" );
}

TEST( FileSystem, join_second_empty )
{
  string s = "aze";
  string s2 = "";
  ASSERT_EQ( FileSystem::join(s, s2), "aze/" );
}

// UserDir should contain prefix parameter
TEST( FileSystem, userdir_cointains_prefix )
{
  string s = FileSystem::getUserDir("rlgl");
  
  ASSERT_NE( s.find("rlgl"), std::string::npos);
}

// Should add a separator at the end 
TEST( FileSystem, userdir_end_separator )
{
  string s = FileSystem::getUserDir("rlgl");
  char end = *s.rbegin();
  ASSERT_TRUE( end == PATH_SEPARATOR);
}

// Shouldn't add separator twice if already present
TEST( FileSystem, userdir_end_separator_twice )
{
  string s = FileSystem::getUserDir("rlgl/");
  ASSERT_EQ( s.find("//"), std::string::npos);
  
}

// Replace é and è with e
TEST( FileSystem, sanitize_1 )
{
  string s = FileSystem::sanitize("aért");
  ASSERT_EQ( s, "aert");
}

TEST( FileSystem, sanitize_2 )
{
  string s2 =FileSystem::sanitize("aèrt");
  ASSERT_EQ( s2, "aert");
}

/* We had an issue with rainbrurpg when trying to create the worlds directory
 * with an inexistant parent.
 *
 */
TEST( FileSystem, createIfNeeded_inexistant_parent )
{
  ASSERT_FALSE(boost::filesystem::exists("~/.incredibly_complex_prefixed_dir"));
  RLGL::System s(".incredibly_complex_prefixed_dir");
  string prefixedHome = s.getPrefixedHome(); // Should create dir
  ASSERT_TRUE(boost::filesystem::exists(prefixedHome));

  // Be sure worlds directory doesn't exist
  string worldsDir = s.getPrefixedHome() + "worlds";
  ASSERT_FALSE(boost::filesystem::exists(worldsDir));
  
  // The following shouldn't throw an exception if s.getPrefixedHome()
  // doesn't exist
  FileSystem::createMapDirectory(prefixedHome, "impossibleMap_name");

  // Be sure the worlds gets created
  ASSERT_TRUE(boost::filesystem::exists(worldsDir));
  ASSERT_TRUE(boost::filesystem::is_directory(worldsDir));

  // Should remove all created dirs
  ASSERT_EQ(boost::filesystem::remove_all(prefixedHome), 3);
  
  // Be sure the directories were removed
  ASSERT_FALSE(boost::filesystem::exists(worldsDir));
  ASSERT_FALSE(boost::filesystem::exists(prefixedHome));
}
