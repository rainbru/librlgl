/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _KEY_EXCEPTION_HPP_
#define _KEY_EXCEPTION_HPP_

#include <stdexcept>
#include <string>

using namespace std;

namespace RLGL
{
  /** A exception used when a HashMap key can't be found or created
    *
    */
  class KeyException : public exception
  {
  public:
    KeyException(const string&,const string&);
    
    const char* what() const noexcept;
    
  private:
    string mWhat;
  };
}

  
#endif // !_KEY_EXCEPTION_HPP_
