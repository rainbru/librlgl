/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FILE_SYSTEM_HPP_
#define _FILE_SYSTEM_HPP_

#include <string>
#include <list>

#include <boost/filesystem.hpp>

#if defined(WIN32) || defined(_WIN32) 
#  define PATH_SEPARATOR '\\'
#else 
#  define PATH_SEPARATOR '/'
#endif 

namespace RLGL
{
  typedef std::list<std::string> tFileList;
  /** Handle filesystem paths
    *
    */
  class FileSystem
  {
  public:
    FileSystem();
    ~FileSystem();

    static std::string getHomeDir();
    static std::string join(const std::string& str1, const std::string& str2,
			    char sep = PATH_SEPARATOR);
    static std::string getUserDir(const std::string&);

    static RLGL::tFileList subdirs(const std::string&);

    static void createPathIfNeeded(const std::string&);
  static std::string createMapDirectory(const std::string&,const std::string&);

    static std::string sanitize(const std::string&);

  protected:
    static void createPathIfNeeded(boost::filesystem::path);

  };
  
}
#endif // !_FILE_SYSTEM_HPP_
