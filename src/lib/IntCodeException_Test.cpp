/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "IntCodeException.hpp"

#include <gtest/gtest.h>
#include <string>

using namespace RLGL;
using namespace std;

class _IntCodeException : public IntCodeException
{
public:
  _IntCodeException(int i, const string& s):
    IntCodeException(i, s)
  {

  }
  
protected:
  virtual string codeToStr(int i)const
  {
    return "1";
  }
};


TEST( IntCodeException, ctor )
{
  _IntCodeException ice(1, "Message");
  EXPECT_EQ( ice.getCode(), 1);
  EXPECT_EQ( ice.getWhat(), "IntCodeException: '1:1' - Message");
}
