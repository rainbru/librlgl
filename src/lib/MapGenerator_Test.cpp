/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "RlglSystem.hpp"
#include "MapGenerator.hpp"

#include <gtest/gtest.h>

using namespace std;
using namespace RLGL;

static RLGL::System sys(".librlgl-tests");

// Just a wrapper to test for protected and private functions
class MapGeneratorWrapper: public MapGenerator
{
public:
  MapGeneratorWrapper():MapGenerator(&sys, 10, 10){}

  string _nStep(const string& s, int i){ return nStep(s, i); };
  string _nStep(const string& s, int i1, int i2){ return nStep(s, i1, i2); };
  void _notifyAddStep(const string& s){ notifyAddStep(s); };

};

// Test if it returns something
TEST( MapGenerator, nStep_1 )
{
  MapGeneratorWrapper mgw;
  string a = mgw._nStep("Test %d", 1);
  ASSERT_EQ( a, "Test 1" );
}

// Test if it returns something
TEST( MapGenerator, nStep_2 )
{
  MapGeneratorWrapper mgw;
  string a = mgw._nStep("Test %d/%d", 1, 14);
  ASSERT_EQ( a, "Test 1/14" );
}

// Setting no name should throw an exception
TEST( MapGenerator, assert_noName )
{
  MapGeneratorWrapper mgw;
  ASSERT_ANY_THROW(mgw.generate());
}

// Setting a name but no seed should throw an exception
TEST( MapGenerator, assert_noSeed )
{
  MapGeneratorWrapper mgw;
  mgw.setMapName("tests");
  ASSERT_ANY_THROW(mgw.generate());
}

TEST( MapGenerator, setSeed_str )
{
  MapGeneratorWrapper mgw;
  mgw.setSeed("1255002");
  ASSERT_EQ(mgw.getSeed(), 1255002);
}

// Should throw an exception if the Seed string can't be converted to integer
TEST( MapGenerator, setSeed_str_error )
{
  MapGeneratorWrapper mgw;
  ASSERT_ANY_THROW(mgw.setSeed("Azef"));
}

// Calling MapGenerator::notifyAddStep with an empty listener list
// shouldn't cause a segfault
TEST( MapGenerator, notify_add_step )
{
  MapGeneratorWrapper mgw;
  mgw._notifyAddStep("Step name");
}

// Should generate a full map
TEST( MapGenerator, full_generate )
{
  MapGeneratorWrapper mgw;
  mgw.setMapName("tests");
  mgw.setSeed("1255002");
  mgw.generate();
}
