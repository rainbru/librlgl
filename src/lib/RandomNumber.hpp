/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef _RANDOM_NUMBER_HPP_
#define _RANDOM_NUMBER_HPP_

#include <vector>
#include <string>
#include <time.h>  // Uses time_t

using namespace std;

namespace RLGL
{
  /** A determinist pseudo-random number geenrator
    *
    * Order of numbers will be the same in all platforms.
    *
    */
  class RandomNumber
  {
  public:
    RandomNumber(time_t seed=0, int version = 0);
    
    static void generate(int);
    int get();
    
  protected:
    string versionToStr(int);
    void load(const string&);
    string filename(int);
    
  private:
    vector<int> mNumbers; //!< The vector of numbers
    int         next;     //!< Next random integer
    int         mVersion; //!< The generator version
  };

}

#endif // !_RANDOM_NUMBER_HPP_
