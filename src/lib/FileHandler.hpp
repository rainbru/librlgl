/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FILE_HANDLER_HPP_
#define _FILE_HANDLER_HPP_

#include <string>
#include <map>

using namespace std;

namespace RLGL
{
  typedef enum{
    FHT_NONE,                //!< No extension or error
    FHT_MAP_DEFINITION,      //!< .md files
    FHT_LANDSCAPE_DEFINITION //!< .ld files
  }tFileHandlerType;
 
  /** A class that handles file types/extensions
    *
    */
  class FileHandler
  {
  public:
    explicit FileHandler(bool autoload  = true);

    void addTypeHandler(const string&, const tFileHandlerType&);
    int size()const;
    bool exists(const string&);
    const string& getExtension(const string&);
    tFileHandlerType getType(const string&);

  private:
    map<string, tFileHandlerType> mMap;
    string lastExtension; // last found extension
  };

}

#endif  // !_FILE_HANDLER_HPP_
