/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ClosingFifoException.hpp"

#include <errno.h>

RLGL::ClosingFifoException::ClosingFifoException(int vCode):
  IntCodeException(vCode, "closing FIFO")
{

}

string
RLGL::ClosingFifoException::codeToStr(int) const
{
  /*

*/
  string ret;
  switch (mCode)
    {
    case EACCES:
      ret = "One of the directories in pathname did not allow search "
	"(execute) permission.";
      break;

    case EDQUOT:
      ret = "The user's quota of disk blocks or inodes on the filesystem "
	"has been exhausted.";
      break;

    case EEXIST:
      ret = "pathname already exists.  This includes the case where "
	"pathname is a symbolic link, dangling or not.";
      break;
      
    case ENAMETOOLONG:
      ret = "Either the total length of pathname is greater than PATH_MAX, "
	"or an individual filename component has a length greater than "
	"NAME_MAX.  In the GNU system, there is no imposed limit on "
	"overall filename length, but some filesystems may place limits "
	"on the length of a component.";
      break;
      
    case ENOENT:
      ret = "A directory component in pathname does not exist or is a "
	"dangling symbolic link.";
      break;

    case ENOSPC:
      ret = "The directory or filesystem has no room for the new file.";
      break;

    case ENOTDIR:
      ret = "A component used as a directory in pathname is not, in fact, a "
	"directory.";
      break;


    case EROFS:
      ret = "pathname refers to a read-only filesystem.";
      break;

      /*
      The following additional errors can occur for mkfifoat():

       EBADF  dirfd is not a valid file descriptor.

       ENOTDIR
              pathname is a relative path and dirfd is a file descriptor
              referring to a file other than a directory.
      */
      
    default:
      ret = "Unknown error";
      
    }
  return ret;
}
