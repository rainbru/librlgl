/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FIFO_OBJECT_HPP_
#define _FIFO_OBJECT_HPP_

#include "Fifo.hpp"

#include "fo/Types.hpp"

namespace RLGL
{
  /** The super-class of all FIFO-serializable objects
    *
    */
  class FifoObject
  {
  public:
    FifoObject(tFifoObjectType);
    virtual ~FifoObject();

    /** Save the object to the given fifo  
      *
      * \return The size of written data in bytes
      *
      */
    virtual ssize_t to_f(RLGL::Fifo&)=0;
    /** Get the object's value from the given fifo  
      *
      * \return The size of read data in bytes
      *
      */
    virtual ssize_t from_f(RLGL::Fifo&)=0;

  protected:
    ssize_t write_type(RLGL::Fifo&);

    /** The type of the Object. see fo/Types.hpp 
      *
      */
    tFifoObjectType type; 
    
  };
}

#endif // !_FIFO_OBJECT_HPP_
