/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef _HASH_MAP_HPP_
#define _HASH_MAP_HPP_

#include "HashMapGroup.hpp"
#include "KeyException.hpp"

#include <stdexcept>
#include <sstream>
#include <map>

using namespace std;

namespace RLGL
{

  /** Here is the HashMap used to store grouped parser values
    *
    * The HashMapGroup is used to create mStringMap keys.
    *
    * If the value you're trying to set isn't derived from std::basic_ostream
    * it will failed at compile time with a "template argument 
    * deduction/substitution failed".
    *
    * To see an example on how to implement a custom object serialization,
    * please see HashMap_Test.cpp.
    *
    */
  class HashMap : public HashMapGroup
  {
  public:
    HashMap();
    virtual ~HashMap();

    template <typename T>
    void set(const string& vKey, const T& vVal)
    {
      string key = keyName(vKey); // Get the grouped key
      map<string, string>::const_iterator it;
      it = mStringMap.find(key);
      if (it != mStringMap.end())
	{
	  // Key was not found
	  throw KeyException(vKey, "key already exist");
	}
      else
	{
	  ostringstream oss;
	  oss << vVal;
	  mStringMap[key] = oss.str();
	}
    }

    /** \param vKey a complete key including groups */
    template <typename T>
    T get(const string& vKey)const
    {
      T ret;
      // Retrieving value using find to avoid key creation using []
      // see  https://stackoverflow.com/a/27580623
      map<string, string>::const_iterator it;
      it = mStringMap.find(vKey);
      if (it == mStringMap.end())
	{
	  // Key was not found
	  throw KeyException(vKey, "can't find key in hashmap");
	}
      else
	{
	  // Key is in the map, extract and return its value
	  string val;
	  istringstream iss(it->second) ;
	  iss >> ret;
	  return ret;
	}
      }
    
  private:
    map<string, string> mStringMap; //!< The key/value map
  };
}
#endif // _HASH_MAP_HPP_
