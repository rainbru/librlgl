/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "HashMap.hpp"

#include <gtest/gtest.h>
#include <iostream>
#include <string>

using namespace std;
using namespace RLGL;

class SerializableObject
{
public:
  int a;
  string b;

  friend ostream& operator<< (ostream &out, SerializableObject const& c)
  {
    return out << c.a << c.b;
  }

  friend istream& operator>> (istream &in, SerializableObject& c)
  {
    return in >> c.a >> c.b ;
  }

};

// Test a string value
TEST( HashMap, str )
{
  HashMap hm;
  hm.set<string>("key", "aze");
  string str = hm.get<string>("key");
  ASSERT_EQ( str, "aze" );
}

TEST( HashMap, grouped_str )
{
  HashMap hm;
  hm.addGroup("gr1");
  hm.set<string>("key", "aze");
  string str = hm.get<string>("gr1::key");
  ASSERT_EQ( str, "aze" );
}

TEST( HashMap, cant_find )
{
  HashMap hm;
  hm.addGroup("gr1");
  hm.set<string>("key", "aze");
  ASSERT_THROW(hm.get<string>("gr1::key2"), KeyException );
}

TEST( HashMap, unserializable )
{
  HashMap hm;
  SerializableObject so;
  so.a = 8;
  so.b = "aze";
  hm.addGroup("gr1");
  hm.set<SerializableObject>("key", so);

  SerializableObject so2 = hm.get<SerializableObject>("gr1::key");
  ASSERT_EQ( so2.a, 8 );
  ASSERT_EQ( so2.b, "aze" );
}

TEST( HashMap, already_exist )
{
  HashMap hm;
  hm.addGroup("gr1");
  hm.set<string>("key", "aze");
  ASSERT_THROW(hm.set<string>("key", "zer"), KeyException );
}
