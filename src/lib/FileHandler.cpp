/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FileHandler.hpp"

RLGL::FileHandler::FileHandler(bool autoload)
{
  if (autoload)
    {
      addTypeHandler(".md", FHT_MAP_DEFINITION);
      addTypeHandler(".ld", FHT_LANDSCAPE_DEFINITION);
    }
}

void
RLGL::FileHandler::addTypeHandler(const string& ext,
				  const RLGL::tFileHandlerType& type)
{
  if (exists(ext))
    throw("Extension already exists");

  mMap[ext] = type;
}

/** Get the number of element conatined in this FileHandler map
  *
  */
int
RLGL::FileHandler::size() const
{
  return mMap.size();
}

/** Tests if a key exists in the map
  *
  */
bool
RLGL::FileHandler::exists(const string& key)
{
  if (mMap.find(key) == mMap.end())
    return false;

  return true;
}

/** Return the file extension if any including the dot
  *
  * *Note* : This is non-standard to return the leading dot.
  *
  */
const string&
RLGL::FileHandler::getExtension(const string& filename)
{
  // Simply search for the last dot in filename
  std::string::size_type idx = filename.rfind('.');
  if(idx != std::string::npos)
    {
      lastExtension = filename.substr(idx);
    }
  else
    {
      // No extension found
      lastExtension = "";
    }
  return lastExtension;
}

RLGL::tFileHandlerType
RLGL::FileHandler::getType(const string& filename)
{
  string ext = getExtension(filename);
  map<string, tFileHandlerType>::const_iterator it;
  it = mMap.find(ext);
  if (it != mMap.end())
    {
      return it->second;
    }
  else
    {
      return FHT_NONE;
    }
}
