/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Seed.hpp"

#include <time.h>       // Uses time 
#include <cstdlib>      // Uses srand, rand
#include <sstream>


using namespace std;

/** Constructor 
  *
  * Create a seed intitialized at 0.
  *
  */
RLGL::Seed::Seed():
  seed(0)
{

}

RLGL::Seed::~Seed()
{

}

/** Set a random seed
  *
  * For instance, the seed is fully random--generated but in the future,
  * it could become a more complex function (world configuration element...).
  *
  */
void
RLGL::Seed::randomize()
{
  srand(time(NULL));
  this->seed = rand();
}

/** Get the underlying integer seed 
  *
  * \return Simply returns seed.
  *
  */
int
RLGL::Seed::to_i() const
{
  return this->seed;
}

/** Get the string representation of the seed
  *
  * \return The seed as a string
  *
  */
string
RLGL::Seed::to_s() const
{
  //  return std::to_string(this->seed);
  std::ostringstream oss;
  oss << this->seed;
  return oss.str();
}
