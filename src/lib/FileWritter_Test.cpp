/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FileWritter.hpp"

#include <gtest/gtest.h>

#include <string>
#include <sstream>

using namespace std;
using namespace RLGL;

// Test if it returnjs something
TEST( FileWritter, full_test )
{
  std::ostringstream oss;
  FileWritter fw(oss);
  fw << "map(\"Test\")" << EOL;
  fw << "{" << EOL;
  ++fw;
  {
    fw << "indented content!" << EOL;
  }
  --fw;
  fw << "}" << EOL;

  string s("map(\"Test\")\n{\n  indented content!\n}\n");
  assert(s ==oss.str());
}

TEST( FileWritter, startGroup )
{
  std::ostringstream oss;
  FileWritter fw(oss);
  fw.startGroup("map(\"Test\")");
  fw << "indented content!";
  string s("map(\"Test\")\n{\n  indented content!");
  assert(s ==oss.str());
}

TEST( FileWritter, endGroup )
{
  std::ostringstream oss;
  FileWritter fw(oss);
  fw.startGroup("map(\"Test\")");
  fw << "indented content!" <<  EOL;
  fw.endGroup();
  string s("map(\"Test\")\n{\n  indented content!\n}\n");
  assert(s ==oss.str());

}

TEST( FileWritter, startNamedGroup )
{
  std::ostringstream oss;
  FileWritter fw(oss);
  fw.startNamedGroup("map", "Test");
  fw << "indented content!";
  string s("map(\"Test\")\n{\n  indented content!");
  assert(s ==oss.str());
}
