/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "HashMapGroup.hpp"

#include <gtest/gtest.h>

using namespace std;
using namespace RLGL;

// Just a wrapper to test for protected and private functions
/*class HashMapGroupWrapper: public MapGenerator
{
public:
  HashMapGroupWrapper():HashMapGroup(){}

  list<string> getGroups()const { return mGroups; };
};
*/

/// Test if group list is empty
TEST( HashMapGroup, empty )
{
  HashMapGroup hmg;
  ASSERT_EQ( hmg.size(), 0 );
}

TEST( HashMapGroup, add )
{
  HashMapGroup hmg;
  hmg.addGroup("gr1");
  ASSERT_EQ( hmg.size(), 1 );
}

TEST( HashMapGroup, pop )
{
  HashMapGroup hmg;
  hmg.addGroup("gr1");
  hmg.addGroup("gr2");
  ASSERT_EQ( hmg.size(), 2 ); // correctly added
  hmg.popGroup();
  ASSERT_EQ( hmg.size(), 1 ); // correctly popped
  
}

TEST( HashMapGroup, current )
{
  HashMapGroup hmg;
  auto c1 = hmg.current();
  ASSERT_EQ( c1, "" );

  hmg.addGroup("gr1");
  auto c2 = hmg.current();
  ASSERT_EQ( c2, "gr1" );

  hmg.addGroup("gr2");
  auto c3 = hmg.current();
  ASSERT_EQ( c3, "gr1::gr2" );

}

TEST( HashMapGroup, keyName_empty )
{
  HashMapGroup hmg;
  auto k = hmg.keyName("aze");
  ASSERT_EQ( k, "aze" );
}

TEST( HashMapGroup, keyName_grouped )
{
  HashMapGroup hmg;
  hmg.addGroup("gr1");
  hmg.addGroup("gr2");
  auto k = hmg.keyName("aze");
  ASSERT_EQ( k, "gr1::gr2::aze" );
}
