/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FILE_WRITTER_HPP_
#define _FILE_WRITTER_HPP_

#include <string>
#include <ostream>
#include <fstream>

#define EOL '\n'

namespace RLGL
{

  /** An helper class to generic stream
    *
    * To write to a file you should use :
    *
    *   ofstream of(mFilename.c_str());
    *   FileWritter fw(of);
    *   fw << "map(\"" << mMapName << "\")" << EOL;
    *   fw << "{" << EOL;
    *   fw++;
    *   {
    *     fw << "Indented content..." << EOL;
    *   }
    *   fw--;
    *   fw << "}" << EOL;
    *  of.close();
    *
    * You may also want to output it to cout :
    *
    *   FileWritter fw(std::cout);
    *
    * Don't try to use endl here, use EOL, you will fire the auto-indent 
    * feature.
    *
    */
  class FileWritter
  {
  public:
    explicit FileWritter(std::ostream&);
    ~FileWritter();

    FileWritter& startGroup(const std::string&);
    FileWritter& startNamedGroup(const std::string&, const std::string&);
    FileWritter& endGroup();
    
    FileWritter& operator++(int); // increment indent
    FileWritter& operator--(int); // decrement indent
    FileWritter& operator++(); // increment indent
    FileWritter& operator--(); // decrement indent

    template <typename T>
    FileWritter& operator<<(const T& obj)
    {
      if (lastChar == EOL)
	indent();
      
      mStream << obj;
      return *this;
    }

    FileWritter& operator<<(char);
    
  protected:
    void indent();

  private:
    std::ostream mStream;
    int indentLevel; // Current indentLevel
    int indentValue; // Value we add when increasing indentation
    char lastChar; 
  };

}

#endif // !_FILE_WRITTER_HPP_
