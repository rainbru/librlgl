/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef _RLGL_SYSTEM_HPP_
#define _RLGL_SYSTEM_HPP_

#include <list>
#include <string>


namespace RLGL
{
  // Forward delarations
  class LocalWorldsListener;
  class MapGenerator;
  // End of forward delarations

  
  /** This class is used to generate local worlds data 
    *
    * This class is a standard PubSub implementation.
    * 
    * The user has to subscribe as a LocalWorldsListener before beeing notified
    * about parsed worlds.
    *
    * Here you also have access to a MapGenerator pointer.
    *
    */
  class System
  {
  public:
    explicit System(const std::string&);
    ~System();

    void subscribe(LocalWorldsListener*);
    void unsubscribe(LocalWorldsListener*);
    void parseLocalWorlds(const std::string&);

    const std::string& getPrefix()const;
    const std::string& getPrefixedHome()const;
    MapGenerator* getGenerator();
    
  protected:
    // Just some LocalWorlds parser functions
    void notifyParsingStarted(int);
    void notifyParsingFinished();
    void notifyNewWorld(const std::string& worldName);
      
  private:
    std::string mPrefix;  //!< The prefix used as home subdirectory
    /** i.e. $HOME/{prefix}/
      *
      * This directory should exist after calling the constructor.
      *
      */
    std::string mPrefixedHomeDir; 

    /** List of listeners */
    std::list<LocalWorldsListener*> mLocalWorldsLiseners;
    MapGenerator* mMapGenerator; //!< The map generator
  };
}

#endif // !_RLGL_SYSTEM_HPP_
