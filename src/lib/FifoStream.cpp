/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FifoStream.hpp"

#include "FifoObject.hpp"

#include "fo/Integer.hpp"
#include "fo/Float.hpp"
#include "fo/String.hpp"

#include <iostream>

using namespace std;

RLGL::FifoStream::FifoStream()
{
  factory.registerObject(FOT_INTEGER, &foInteger::factoryMethod);
  factory.registerObject(FOT_STRING,  &foString::factoryMethod);
  factory.registerObject(FOT_FLOAT,   &foFloat::factoryMethod);
}

RLGL::FifoStream::~FifoStream()
{

}

unsigned int
RLGL::FifoStream::size() const
{
  return mMap.size();
}

RLGL::FifoStream&
RLGL::FifoStream::push(std::string key, FifoObject* obj)
{
  mMap[key] = obj;
  return *this;
}

RLGL::FifoObject*
RLGL::FifoStream::get(const std::string& key)
{
  return mMap[key];
}

/**
 * \return The number of written bytes
 */
ssize_t
RLGL::FifoStream::write(RLGL::Fifo& f)
{
  ssize_t written = 0;
  
  // Write the map's size
  written += f.write<int>(mMap.size());
  
  std::map<std::string, FifoObject*>::iterator it;
  for (it=mMap.begin(); it!=mMap.end(); ++it)
    {
      written += f.write(it->first); // First write the key
      written += it->second->to_f(f); // Then, its value
    }
  return written;
}

/**
  * \return The number of read bytes
  */
ssize_t 
RLGL::FifoStream::read(RLGL::Fifo& f)
{
  int mapSize;
  string key;
  ssize_t written = 0;
  foType t;
  
  // Write the map's size
  written += f.read<int>(&mapSize);
  cout << "Reading map with size " << mapSize << endl;

  for (int idx=0; idx < mapSize; ++idx)
    {
      written += f.read(key);   // First, read the key
      cout << "Reading map key '" << key << "'" << endl;
      written += t.from_f(f);   // Then, the object type
      cout << "Object type is '" << (int)t.value() << "'" << endl;

      FifoObject* fo = factory.create(t);
      cout << "Created object from factory" << endl;
      
      written += fo->from_f(f);
      cout << "Called fo->from_f" << endl;
      
      mMap[key] = fo;
    }
  
  return written;
}
