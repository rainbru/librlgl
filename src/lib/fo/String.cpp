/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fo/String.hpp"

RLGL::foString::foString():
  FifoObject(FOT_STRING),
  mString("")
{

}

RLGL::foString::foString(const string& v):
  FifoObject(FOT_STRING),
  mString(v)
{

}

RLGL::foString::~foString()
{

}

ssize_t
RLGL::foString::to_f(RLGL::Fifo& f)
{
  ssize_t w = FifoObject::write_type(f);
  w += f.write(mString);
  return w;
}

ssize_t
RLGL::foString::from_f(RLGL::Fifo& f)
{
  return f.read(mString);
}

const string&
RLGL::foString::value() const
{
  return mString;
}

void
RLGL::foString::setValue(const string& v)
{
  mString = v;
}
    
RLGL::FifoObject*
RLGL::foString::factoryMethod()
{
  return new foString();
}
