/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fo/Float.hpp"


RLGL::foFloat::foFloat():
  FifoObject(FOT_FLOAT),
  mFloat(0.0)
{

}

RLGL::foFloat::foFloat(float vFloat):
  FifoObject(FOT_FLOAT),
  mFloat(vFloat)
{

}

RLGL::foFloat::~foFloat()
{

}

ssize_t
RLGL::foFloat::to_f(RLGL::Fifo& f)
{
  ssize_t w = FifoObject::write_type(f);
  w += f.write<float>(mFloat);
  return w;

}

ssize_t
RLGL::foFloat::from_f(RLGL::Fifo& f)
{
  return f.read<float>(&mFloat);
}

float
RLGL::foFloat::value() const
{
  return mFloat;
}

void
RLGL::foFloat::setValue(float vFloat)
{
  mFloat = vFloat;
}

RLGL::FifoObject*
RLGL::foFloat::factoryMethod()
{
  return new foFloat();
};
