/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fo/Type.hpp"

#include "FifoStream.hpp"

#include <gtest/gtest.h>
#include <iostream>

using namespace RLGL;
using namespace std;

TEST( foType, ctor )
{
  foType fs;
  EXPECT_EQ(fs.value(), 0);
  EXPECT_EQ(fs.value(), FOT_UNDEFINED);
}

TEST( foType, ctor_value )
{
  foType fs(FOT_FLOAT);
  EXPECT_EQ(fs.value(), FOT_FLOAT);
}

TEST( foType, set_value )
{
  foType fs;
  fs.setValue(FOT_FLOAT);
  EXPECT_EQ(fs.value(), FOT_FLOAT);
}

// Check the written size of a type
TEST( foType, write_size )
{
  string path = "/tmp/write_read-test";
  Fifo fr(path);
  fr.open(READ);

  Fifo fw(path);
  fw.open(WRITE);

  // Writing
  FifoStream fs;
  foType ft(FOT_STRING);
  fs.push("type", &ft);
  size_t written = fs.write(fw);
  EXPECT_EQ(written, 17);
}
