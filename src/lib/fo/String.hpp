/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FO_STRING_HPP_
#define _FO_STRING_HPP_

#include "FifoObject.hpp"
#include "Fifo.hpp"

#include <string>

using namespace std;

namespace RLGL
{
  /** The super-class of all FIFO-serializable objects
    *
    */
  class foString : public FifoObject
  {
  public:
    foString();
    explicit foString(const string&);
    static FifoObject* factoryMethod();

    virtual ~foString();

    /** Save the object to the given fifo  */
    virtual ssize_t to_f(RLGL::Fifo&);
    /** Get the object's value from the given fifo  */
    virtual ssize_t from_f(RLGL::Fifo&);

    const string& value() const;
    void setValue(const string&);
    
  private:
    string mString;
  };
}

#endif // !_FO_STRING_HPP_
