/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FO_TYPES_HPP_
#define _FO_TYPES_HPP_

#include <cstdint> // Uses uint8_t

/** The type for the FifoObject
  *
  */
enum FifoObjectType
{
  FOT_UNDEFINED = 0, //!< Will raise error
  
  FOT_INTEGER,       //!< for Integer type
  FOT_FLOAT,         //!< for Float
  FOT_STRING         //!< for String type
};

/** Be sure to serialize it in 8 bits integer
  *
  */
typedef uint8_t tFifoObjectType;



#endif // !_FO_TYPES_HPP_
