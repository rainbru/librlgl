/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fo/Float.hpp"

#include <gtest/gtest.h>

using namespace RLGL;

TEST( foFloat, ctor )
{
  foFloat fs;
  EXPECT_EQ(fs.value(), 0.0);
}

// Can we create a float from an integer
TEST( foFloat, ctor_int )
{
  foFloat fs(25);
  EXPECT_FLOAT_EQ(fs.value(), 25);
}

TEST( foFloat, float_ctor )
{
  foFloat fs(25.12);
  EXPECT_FLOAT_EQ(fs.value(), 25.12);
}

TEST( foFloat, setValue )
{
  foFloat fs;
  fs.setValue(95.12);
  EXPECT_FLOAT_EQ(fs.value(), 95.12);
}
