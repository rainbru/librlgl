/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Integer.hpp"

RLGL::foInteger::foInteger():
  FifoObject(FOT_INTEGER),
  mInt(0)
{

}

RLGL::foInteger::foInteger(int i):
  FifoObject(FOT_INTEGER),
  mInt(i)
{

}


RLGL::foInteger::~foInteger()
{

}


ssize_t
RLGL::foInteger::to_f(RLGL::Fifo& f)
{
  ssize_t w = FifoObject::write_type(f);
  w += f.write<int>(mInt);
  return w;
}


ssize_t
RLGL::foInteger::from_f(RLGL::Fifo& f)
{ 
  return f.read<int>(&mInt);
}

int
RLGL::foInteger::value() const
{
  return mInt;
}

void
RLGL::foInteger::setValue(int i)
{
  mInt = i;
}


RLGL::FifoObject*
RLGL::foInteger::factoryMethod()
{
  return new foInteger();
}
