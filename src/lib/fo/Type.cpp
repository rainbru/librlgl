/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fo/Type.hpp"

RLGL::foType::foType():
  FifoObject(FOT_UNDEFINED),
  mValue(0)
{

}

RLGL::foType::foType(uint8_t value):
  FifoObject(FOT_UNDEFINED),
  mValue(value)
{

}


RLGL::foType::~foType()
{

}

ssize_t
RLGL::foType::to_f(RLGL::Fifo& f)
{
  // No need to write our own type
  return f.write<uint8_t>(mValue);
}

ssize_t
RLGL::foType::from_f(RLGL::Fifo& f)
{
  // No need to read our own type
  return f.read<uint8_t>(&mValue);
}

uint8_t
RLGL::foType::value() const
{
  return mValue;
}

void
RLGL::foType::setValue(uint8_t v)
{
  mValue = v;
}
