/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FO_INTEGER_HPP_
#define _FO_INTEGER_HPP_

#include "FifoObject.hpp"
#include "Fifo.hpp"

namespace RLGL
{
  /** The super-class of all FIFO-serializable objects
    *
    */
  class foInteger : public FifoObject
  {
  public:
    foInteger();
    explicit foInteger(int);
    static FifoObject* factoryMethod();

    virtual ~foInteger();

    /** Save the object to the given fifo  */
    virtual ssize_t to_f(RLGL::Fifo&);
    /** Get the object's value from the given fifo  */
    virtual ssize_t from_f(RLGL::Fifo&);

    int value() const;
    void setValue(int);
    
  private:
    int mInt;
  };
}

#endif // !_FO_INTEGER_HPP_
