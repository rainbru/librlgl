/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _FO_TYPE_HPP_
#define _FO_TYPE_HPP_

#include "FifoObject.hpp"
#include "Fifo.hpp"

#include <cstdint> // Uses uint8_t

using namespace std;

namespace RLGL
{
  /** A specialized class for unsigned int8
    *
    * Mainly used for Object type.
    *
    */
  class foType : public FifoObject
  {
  public:
    foType();
    foType(uint8_t);

    virtual ~foType();

    virtual ssize_t to_f(RLGL::Fifo&);
    virtual ssize_t from_f(RLGL::Fifo&);

    uint8_t value() const;
    void setValue(uint8_t);
    
  private:
    uint8_t mValue;
  };
}

#endif // !_FO_TYPE_HPP_
