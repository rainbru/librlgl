# This file will define:
#
# - LIBRLGL_INCLUDES:  the needed include directories (i.e. -I);
# - LIBRLGL_LIBRARIES: the needed libraries (i.e. ldflags or -l).
#

find_package(PkgConfig)
pkg_search_module(GIT2 REQUIRED libgit2)

# Enables C++11
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
else()
  message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

# Search for boost components
#   We need Boost.Spirit but can't search for a header-only library
set(BOOST_COMPONENTS system filesystem program_options thread)
find_package(Boost REQUIRED COMPONENTS ${BOOST_COMPONENTS})
  
include_directories(${GIT2_INCLUDES} ${Boost_INCLUDE_DIRS})
add_library(rlgl SHARED
  ${CMAKE_CURRENT_LIST_DIR}/ClosingFifoException.cpp
  ${CMAKE_CURRENT_LIST_DIR}/CreateFifoException.cpp
  ${CMAKE_CURRENT_LIST_DIR}/Fifo.cpp
  ${CMAKE_CURRENT_LIST_DIR}/FifoObject.cpp
  ${CMAKE_CURRENT_LIST_DIR}/FifoObjectFactory.cpp
  ${CMAKE_CURRENT_LIST_DIR}/FifoStream.cpp
  ${CMAKE_CURRENT_LIST_DIR}/FileHandler.cpp
  ${CMAKE_CURRENT_LIST_DIR}/FileSystem.cpp
  ${CMAKE_CURRENT_LIST_DIR}/FileWritter.cpp
  ${CMAKE_CURRENT_LIST_DIR}/HashMap.cpp
  ${CMAKE_CURRENT_LIST_DIR}/HashMapGroup.cpp
  ${CMAKE_CURRENT_LIST_DIR}/IndexFile.cpp
  ${CMAKE_CURRENT_LIST_DIR}/IntCodeException.cpp
  ${CMAKE_CURRENT_LIST_DIR}/KeyException.cpp
  ${CMAKE_CURRENT_LIST_DIR}/MapGenerator.cpp
  ${CMAKE_CURRENT_LIST_DIR}/MapParser.cpp
  ${CMAKE_CURRENT_LIST_DIR}/ParserBase.cpp
  ${CMAKE_CURRENT_LIST_DIR}/RandomNumber.cpp
  ${CMAKE_CURRENT_LIST_DIR}/RlglSystem.cpp
  ${CMAKE_CURRENT_LIST_DIR}/Seed.cpp
  
  ${CMAKE_CURRENT_LIST_DIR}/fo/Float.cpp
  ${CMAKE_CURRENT_LIST_DIR}/fo/Integer.cpp
  ${CMAKE_CURRENT_LIST_DIR}/fo/String.cpp
  ${CMAKE_CURRENT_LIST_DIR}/fo/Type.cpp
  )
TARGET_LINK_LIBRARIES(rlgl ${GIT2_LIBRARIES}  ${Boost_LIBRARIES})

SET(LIBRLGL_INCLUDES ${GIT2_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
    ${CMAKE_CURRENT_LIST_DIR})
 
SET(LIBRLGL_LIBRARIES ${GIT2_LIBRARIES} ${Boost_LIBRARIES} rlgl)
