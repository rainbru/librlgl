/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "IndexFile.hpp"

#include <boost/filesystem.hpp>

#include <gtest/gtest.h>
#include <string>

using namespace RLGL;
using namespace std;

// Test if no autoload gets an empty typehandler
TEST( IndexFile, filename )
{
  // The second argument here is just used when creating the .md file
  // The first argument, comming from MapGenerator::generate already contains
  // the prefixed map directory!
  IndexFile fi("/worlds", "map1");
  string s = "/worlds/" FILENAME;
  EXPECT_EQ( fi.getFilename(), s);
}

// Test if indexfile is finally generated
TEST( IndexFile, generate )
{
  IndexFile fi(".", "index_file_test");
  fi.generate();
  EXPECT_TRUE(boost::filesystem::exists(fi.getFilename()));
  boost::filesystem::remove(fi.getFilename());
}
