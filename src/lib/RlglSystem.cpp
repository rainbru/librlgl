/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** \file RlglSystem.cpp
  * RLGL::Sstem implementation
  *
  */

#include "RlglSystem.hpp"

#include "LocalWorldsListener.hpp"
#include "FileSystem.hpp"
#include "MapGenerator.hpp"

#include <iostream>

using namespace std;
using namespace RLGL;

/** Constructor of a prefixed System
  *
  * \param prefix The prefix directory. If it doesn't exist, it will be 
  *        created.
  *
  */
RLGL::System::System(const std::string& prefix):
  mPrefix(prefix),
  mPrefixedHomeDir(FileSystem::getUserDir(prefix))
{

  FileSystem::createPathIfNeeded(mPrefixedHomeDir);
  mMapGenerator = new MapGenerator(this, 10, 10);

}

/** The destructor
  *
  */
RLGL::System::~System()
{
  delete mMapGenerator;
}

/** Adds a listener to the local worlds listener list
  *
  * \param lwl the LocalWorldsListener to be added.
  *
  */
void
RLGL::System::subscribe(LocalWorldsListener* lwl)
{
  mLocalWorldsLiseners.push_back(lwl);
}

/** Removes a listener to the local worlds listener list
  *
  * \param lwl the LocalWorldsListener to be removed.
  *
  */
void
RLGL::System::unsubscribe(LocalWorldsListener* lwl)
{
  mLocalWorldsLiseners.remove(lwl);
}

/** Notify all listeners that the parsing started
  *
  * \param nbWorlds The number of worlds to be parsed.
  *
  */
void
RLGL::System::notifyParsingStarted(int nbWorlds)
{
  std::list<LocalWorldsListener*>::iterator p;
  for(p = mLocalWorldsLiseners.begin(); p != mLocalWorldsLiseners.end(); ++p)
    (*p)->parsingStarted(nbWorlds);
}

/** Notify all listeners that the parsing is finished
  *
  */
void
RLGL::System::notifyParsingFinished()
{
  std::list<LocalWorldsListener*>::iterator p;
  for(p = mLocalWorldsLiseners.begin(); p != mLocalWorldsLiseners.end(); ++p)
    (*p)->parsingFinished();
}

/** Notify all listeners that a new world was found
  *
  * \param worldName The world's name
  *
  */
void
RLGL::System::notifyNewWorld(const string& worldName)
{
  std::list<LocalWorldsListener*>::iterator p;
  for(p = mLocalWorldsLiseners.begin(); p != mLocalWorldsLiseners.end(); ++p)
    (*p)->gotWorld(worldName);
}

/**
  * 
  * \param prefix can be the project name (i.e. ".rainbrurpg")
  *
  */
void
RLGL::System::parseLocalWorlds(const std::string& prefix)
{
  tFileList fl;
  
  // First get the new of to-be-parsed worlds
  try
    {
      string ud = FileSystem::getUserDir(prefix);
      string worldsdir = FileSystem::join(ud, "worlds");
      fl = FileSystem::subdirs(worldsdir);
      notifyParsingStarted(fl.size());
    }
  catch (...)
    {
      // Can't find userDir = 0 worlds
      notifyParsingStarted(0);
      return;
    }

  // Then parse each fl item
  tFileList::iterator it;
  for(it = fl.begin(); it != fl.end(); ++it)
    {
      // TODO: Not parsing yet, only notify pathname
      notifyNewWorld(*it);
    }

  // Parsing is ended
  notifyParsingFinished();
}

/** Returns the current generator
  *
  * \returns the current generator.
  *
  */
RLGL::MapGenerator*
RLGL::System::getGenerator()
{
  return mMapGenerator;
}

/** Returns the prefix string
  *
  * \returns the prefix.
  *
  */
const string&
RLGL::System::getPrefix()const
{
  return mPrefix;
}

/** Returns the prefixed home directory
  *
  * \returns the prefixed home directory.
  *
  */
const std::string&
RLGL::System::getPrefixedHome()const
{
  return mPrefixedHomeDir;
}
