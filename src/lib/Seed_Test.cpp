/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Seed.hpp"

#include <gtest/gtest.h>

using namespace RLGL;

// Test if a newly created seed is zero-initialised
TEST( Seed, zero )
{
  Seed s;
  EXPECT_EQ( s.to_i(), 0 );
}

// Test if a randomized seed isn't null
TEST( Seed, randomize )
{
  Seed s;
  s.randomize();
  EXPECT_NE( s.to_i(), 0 );
}

// Test that randomize() change the current seed value
TEST( Seed, randomize_2 )
{
  Seed s;
  int i1 = s.to_i();
  s.randomize();
  int i2 = s.to_i();
  EXPECT_NE( i1, i2 );
}
