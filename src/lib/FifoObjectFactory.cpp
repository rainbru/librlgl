/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FifoObjectFactory.hpp"

 
/** Return the number of registered objects */
size_t
RLGL::FifoObjectFactory::size() const
{
   
  return registeredTypes.size();
}


void
RLGL::FifoObjectFactory::registerObject(foType t, factoryMethod m)
{
  std::map<uint8_t, factoryMethod>::iterator registeredPair =
    registeredTypes.find(t.value());

  if(registeredPair == registeredTypes.end())
    registeredTypes[t.value()] =  m;
  else
    throw "Already registered";
}

RLGL::FifoObject*
RLGL::FifoObjectFactory::create(foType t)
{
  std::map<uint8_t, factoryMethod>::iterator registeredPair =
    registeredTypes.find(t.value());
  
  // did we find one?
  if(registeredPair == registeredTypes.end())
    throw "Can't find object type";

  // return a new instance of derived class
  return registeredPair->second();
}
