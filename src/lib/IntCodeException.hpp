/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _INT_CODE_EXCEPTION_HPP_
#define _INT_CODE_EXCEPTION_HPP_

#include <stdexcept>
#include <string>

using namespace std;

namespace RLGL
{
  class IntCodeException : public exception
  {
  public:
    IntCodeException(int, const string&);

    int getCode() const;
    const string& getWhat();

    virtual const char* what() const throw (){
      return mWhat.c_str();
    }

    
  protected:
    virtual string codeToStr(int) const =0;

    int mCode;
    string mWhat;
  };
}

#endif // !_INT_CODE_EXCEPTION_HPP_
