/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of librlgl.
 *
 * librlgl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librlgl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librlgl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** \mainpage libRLGL
  *
  * \section intro_sec Introduction
  * This the librlgl documentation main page. You may want to start
  * reading RLGL::System class documentation.
  *
  */

#include <boost/program_options.hpp>
#include <iostream>
#include <QApplication>

#include "config.h"

#include "src/lib/RlglSystem.hpp"
#include "src/lib/MapGenerator.hpp"
#include "src/lib/RandomNumber.hpp"

#include "Preview.hpp"

namespace po = boost::program_options;
using namespace std;

int
main(int argc, char** argv)
{
  QApplication app(argc, argv);

  // Declare the supported options.
  po::options_description generic("Generic options");
  generic.add_options()
    ("version,v", "print version string")
    ("help", "produce help message")    
    ;
 
po::options_description global("Global options");
global.add_options()
    ("debug", "Turn on debug output")
    ("command", po::value<std::string>(), "command to execute")
    ("subargs", po::value<std::vector<std::string> >(), "Arguments for command");

po::positional_options_description pos;
pos.add("command", 1).
    add("subargs", -1);

po::variables_map vm;

po::parsed_options parsed = po::command_line_parser(argc, argv).
  options(generic).
  options(global).
  positional(pos).
  allow_unregistered().
  run();

 po::store(parsed, vm);
 std::string cmd;
 try
   {
     cmd = vm["command"].as<std::string>();
   }
 catch (...)
   {
     cout << "Please supply a command or 'help'" << endl;
   }
 if (cmd == "help")
   {
     cout << generic
	  << global
	  << endl;
   }
 else if (cmd == "numbers")
   {
     /* Generate a bunch of real random numbers
      * Aim: to be saved to a versionned text file to
      * be loaded for further generation.
      *
      */
     RLGL::RandomNumber::generate(100);
   }
 else if (cmd == "generate")
   {
     RLGL::System sys(".rainbrurpg");
     RLGL::MapGenerator(&sys, 250, 250);

     // ls command has the following options:
    po::options_description ls_desc("ls options");
    ls_desc.add_options()
        ("hidden", "Show hidden files")
        ("path", po::value<std::string>(), "Path to list");

    // Collect all the unrecognized options from the first pass. This will
    // include the command name, so we need to erase that.
    std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
    opts.erase(opts.begin());

    // Parse again...
    po::store(po::command_line_parser(opts).options(ls_desc).run(), vm);
   }
 else if (cmd == "preview")
   {
     po::options_description ls_desc("preview options");
     ls_desc.add_options()
       ("filename", po::value<std::string>(), "File to preview");

     std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
     opts.erase(opts.begin());
     po::store(po::command_line_parser(opts).options(ls_desc).run(), vm);

     try{
       Preview pv(vm["filename"].as<std::string>(), argc, argv);
       return app.exec();
     }
     catch (...){
       cerr << "You must use --filename option" << endl;
     }
   }

 /*  
  if (vm.count("help")) {
    cout << generic << endl;
    return 1;
  }

  if (vm.count("version")) {
    cout << PACKAGE << " " << VERSTRING << endl;
  }
    */
}
