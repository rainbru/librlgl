# librlgl

The *Rainbru's  Landscape and Geometry Langage* : a base library for
RainbruRPG's graphical world.

# Build

If cmake can't find qt5 develoment file :

	sudo apt install qtbase5-dev qtbase5-dev-tools libgit2-dev libboost-dev \
	  libboost-system-dev libboost-filesystem-dev libboost-program-options-dev \
	  libboost-thread-dev libgtest-dev lcov

## Manually building gtest

	#cd /usr/src/gtest
    #mkdir build
    #cd build
    #cmake ..
    #make
    #make install

# Concept

Defines a library that can be used to :

* Declare a complete 3D world in text format;
* Ship a compiler for this/these language(s) to render it in 2D or 3D.
* Use binary blobs eventually to serve as cache.

We don't use heightmaps.

## Map generation.

Generate an hexagon-based map and :

- Each of the used function become a langage command, possibly with a seed
  command when a random thing is needed. It gives us the *LDL* aka
  Landscape Declarative Language.
- The client compiler takes the LDL file and generate a binary model
  (possibly in Ogre3D native format).

# Using as a submodule

Adding as a submodule in ext/librlgl :

	mkdir ext/
	cd ext/
	git submodule add git@github.com:rainbru/librlgl.git

The next step is to include it from you project CMakeLists.txt :

	include("${PROJECT_SOURCE_DIR}/ext/librlgl/src/lib/CMakeLists.txt")


You'll have to define ROOTDIR to be able to compile :

	add_definitions(-DROOTDIR="${CMAKE_CURRENT_SOURCE_DIR}/ext/librlgl")

