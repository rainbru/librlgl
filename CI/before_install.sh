#!/bin/sh -e  

if [[ "$TRAVIS_OS_NAME" == "osx" ]];
then
    # Building on OSX
    # For a list of available formulae:
    #   https://travis-ci.org/rainbru/librlgl/jobs/283777947
    brew update    # Try to fix "Homebrew must be run under Ruby 2.3!"
    brew install qt5
    echo $PKG_CONFIG_PATH
    PKG_CONFIG_PATH=/usr/local/opt/qt/lib/pkgconfig
    export PKG_CONFIG_PATH
else
    # Building on GNU/Linux
    
    # For clang-3.8
    sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu/ trusty-updates main universe"

    sudo apt-get update -qq

    # Here I use --force-yes due to llvm signature errors
    sudo apt-get --yes install mercurial libgit2-dev libboost-dev\
	 libboost-program-options-dev libboost-system-dev \
	 libboost-filesystem-dev \
	 libboost-graph-dev libboost-test-dev libenet-dev guile-2.0-dev \
	 clang clang-3.4 libgtest-dev google-mock libqt5widgets5 \
	 libboost-thread-dev qtbase5-dev libqt5widgets5 lcov curl
    
    ## Handle linking from /usr/local
    echo "/usr/local/lib" | sudo tee /etc/ld.so.conf.d/local-lib.conf
    sudo ldconfig

    # build libgtest & libgtest_main
    sudo mkdir /usr/src/gtest/build
    sudo chmod a+w /usr/src/gtest/build
    cd /usr/src/gtest/build
    cmake .. -DBUILD_SHARED_LIBS=1
    make -j4
    sudo ln -s /usr/src/gtest/build/libgtest.so /usr/lib/libgtest.so
    sudo ln -s /usr/src/gtest/build/libgtest_main.so /usr/lib/libgtest_main.so
fi
