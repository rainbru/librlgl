#!/bin/sh -e

# The '-e' flag in the shebang should make fail the script if
# an error occurs.

if [[ "$TRAVIS_OS_NAME" == "osx" ]];
then
    # Building on OSX
    PKG_CONFIG_PATH=/usr/local/opt/qt/lib/pkgconfig
    export PKG_CONFIG_PATH
fi

echo $PKG_CONFIG_PATH
mkdir ./build
cd build
cmake ..
VERBOSE=1 make
make check
